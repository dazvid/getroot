# Introduction

This is a document to help with various penetration testing tasks, including:

- [Scanning & Enumeration](./Scanning_and_Enumeration.md)
- Initial foothold
  - A process for analysing web applications vulnerabilities
  - Identifying low hanging fruit
  - Exploiting
- Post exploitation
  - Host enumeration
  - Interesting file locations
  - Privilege escalation
  - Persistence
