# Overview

Generally in a blackbox testing scenario, we need to identify the services that are accessible, and which versions the software is. This can include things like:

- Web server version
- Web application version
- FTP servers
- Administrative services
  - ssh
  - RDP
  - VNC
  - WinRM
- Custom applications on high ports

We can utilise common open source tools to identify and fingerprint these services.

## nmap

The classic port scanner and fingerprinting tool is **nmap**.

### nmap examples

```bash,editable
nmap -sS -p- --min-rate 1000 --max-retries 2 -oA fullscan 10.1.1.1
```

## Mapping web sites and applications

### gobuster

Not installed by default with kali linux (at the time of writing). Install with `apt install gobuster`.

#### gobuster examples

To brute force directories of the **website** `http://10.1.1.1` using the **wordlist** `/usr/share/dirbuster/medium_2.3_something.txt` with **20 threads** and automatically appending the **file extensions** `txt` and `php`:
`gobuster dir -w /usr/share/dirbuster/medium_2.3_something.txt -u http://10.1.1.1 -t 20 -x txt,php`

## Fingerprinting web application versions

### whatweb

### wappalyzer (browser plugin)
